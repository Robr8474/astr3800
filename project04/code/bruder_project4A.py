from astropy.io import fits
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import scipy.stats as stats

def linfit(xvalues, yvalues):

    """
    Calculate best fit line for 2 arrays.

    Input Parameters
    ----------
    xvalues : numpy array of int or float
        array of x values
    yvalues : numpy array of int or float
        array of y values

    Example
    -------
    To compute the best fit line and error:
    >>> A2, A, B, deltaY, deltaA, deltaB, chi2, pvalue = linfit(xvalues, yvalues)

    Returns
    -------
    Slope with y-intercept = 0 : float
    Slope : float
    y-intercept : float
    sigma y : float
    sigma A (intercept not 0) : float
    sigma B : float
    chi squared : float
    p-value : float
    """

    x = np.vstack((np.ones(len(xvalues)), xvalues))
    
    x0 = xvalues.reshape((len(xvalues),1))
    xvalues2 = xvalues.reshape((1,len(xvalues)))

    #import pdb
    #pdb.set_trace()
    A = np.dot(np.linalg.inv(np.dot(x, x.T)), np.dot(yvalues, x.T))

    A2 =  np.dot(np.linalg.inv(np.dot(xvalues2, x0)), np.dot(yvalues, x0))
    
    deltaY = np.sqrt((1 / (float(len(yvalues)) - 2) * np.sum((yvalues - (A[1] * xvalues + A[0]))**2)))

    delta = (float(len(xvalues)) * np.sum(xvalues**2)) - (np.sum(xvalues)**2)

    deltaA = deltaY * np.sqrt(np.sum(xvalues**2) / delta)

    deltaB = deltaY * np.sqrt(float(len(yvalues)) / delta)

    chi2 = np.sum((yvalues - (A[1] * xvalues + A[0]))**2 / np.abs((A[1] * xvalues + A[0])))

    pvalue = 1 - stats.chi2.cdf(chi2, (len(yvalues)-2))

    #chi22, pvalue = stats.chisquare(yvalues, exp)

    return A2, A[1], A[0], deltaY, deltaA, deltaB, chi2, pvalue


#-------------------------------------------------------------------------
#retrieve data

hubbleData = np.genfromtxt('../data/hubbleoriginal.csv', delimiter=',', dtype=float, skip_header=1)

hubData = hubbleData.T

#scatter plot data

pl.scatter(hubData[1], hubData[2])
pl.ylabel('Recession Velocity (km/s)')
pl.xlabel('Dist (Mpc)')
pl.title('Hubbles Original Data')
pl.savefig('../images/bruder_project4A1.png')
pl.show()

#compute best fit line

A2, A, B, deltaY, deltaA, deltaB, chi22, pvalue = linfit(hubData[1], hubData[2])

#plot data with best fit line

pl.scatter(hubData[1], hubData[2])
pl.plot(hubData[1], (A * hubData[1] + B))
pl.plot(hubData[1], ((A + deltaA) * hubData[1] + B + deltaB))
pl.plot(hubData[1], ((A - deltaA) * hubData[1] + B - deltaB))
#pl.plot(hubData[1], (A2 * hubData[1]))
pl.ylabel('Recession Velocity (km/s)')
pl.xlabel('Dist (Mpc)')
pl.title('Hubbles Original Data')
pl.legend(['Best Fit', '68% Upper Limit', '68% Lower Limit'], loc='upper left', shadow=True, fontsize='medium')

pl.tight_layout()
pl.savefig('../images/bruder_project4A2.png')
pl.show()

print A, deltaA, B, A2
print chi22, pvalue
#-------------------------------------------------------------------------

