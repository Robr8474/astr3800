from astropy.io import fits
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import scipy.stats as stats
from bruder_project4A import linfit

def hubfit(dist, vel, RA, DEC):

    """
    Calculate Hubble's correction for solar motion and Ho.

    Input Parameters
    ----------
    dist : numpy array of int or float
        array of distances (Mpc)
    vel : numpy array of int or float
        array of velocities (km/s)
    RA : numpy array of int or float
        array of right ascension values (radians)
    DEC : numpy array of int or float
        array of declination values (radians)
    Example
    -------
    To compute Hubble's correction and errors:
    >>> H, x, y, z, deltaY, deltaA, deltaB, chi22, pvalue = hubfit(hubData[1], hubData[2], RA, DEC)

    Returns
    -------
    Ho : float
    x : float
    y : float
    z : float
    sigma A : float
    sigma B : float
    chi squared : float
    p-value : float

    """
    a = np.cos(RA) * np.cos(DEC)
    b = np.sin(RA) * np.cos(DEC)
    c = np.sin(DEC)

    x = np.vstack((dist, a, b, c))
    
    A = np.dot(np.linalg.inv(np.dot(x, x.T)), np.dot(vel, x.T))

    deltaY = np.sqrt((1 / (float(len(vel)) - 2) * np.sum((vel - (A[1] * dist + A[0]))**2)))

    delta = (float(len(dist)) * np.sum(dist**2)) - (np.sum(dist)**2)

    deltaA = deltaY * np.sqrt(np.sum(dist**2) / delta)

    deltaB = deltaY * np.sqrt(float(len(vel)) / delta)

    chi2 = np.sum((vel - (A[0] * dist))**2 / (A[0] * dist))

    pvalue = 1 - stats.chi2.cdf(chi2, len(dist))

    return A[0], A[1], A[2], A[3], deltaY, deltaA, deltaB, chi2, pvalue


#-------------------------------------------------------------------------
#retrieving data

hubbleData = np.genfromtxt('../data/hubbleoriginal.csv', delimiter=',', dtype=float, skip_header=True)

hubData = hubbleData.T

#convert to radians

RA = np.array(hubData[3] * 3.14 / 12)
DEC = np.array(hubData[4] * 3.14 / 180)

#calculate constants and errors

H, x, y, z, deltaY, deltaA, deltaB, chi2, pvalue = hubfit(hubData[1], hubData[2], RA, DEC)

#compute corrected velocities

vel = hubData[2] - x * (np.cos(RA) * np.cos(DEC)) - y * (np.sin(RA) * np.cos(DEC)) - z * (np.sin(DEC))

chi2 = np.sum((vel - (H * hubData[1]))**2 / (H * hubData[1]))

pvalue = 1 - stats.chi2.cdf(chi2, len(hubData[1]))

print H, x, y, z
print chi2, pvalue

#----------------------------------------------------------------------------
#plot of corrected velocities and hubble constant
pl.scatter(hubData[1], vel)
pl.plot(hubData[1], (H * hubData[1]))

pl.title('Corrected Velocity')
pl.xlabel('Dist (Mpc)')
pl.ylabel('Z * c')
pl.legend(['Ho = 465'], loc='upper left', shadow=True, fontsize='medium')
pl.plot(hubData[1], (H * hubData[1]))
pl.savefig('../images/bruder_project4C.png')
pl.show()

f, axarr = pl.subplots(2, figsize = (10, 10))
axarr[0].scatter(hubData[1], hubData[2])
axarr[0].plot(hubData[1], (H * hubData[1]))

axarr[0].set_title('Uncorrected Velocity')
axarr[0].set_xlabel('Dist (Mpc)')
axarr[0].set_ylabel('Z * c')
axarr[0].legend(['Ho'], loc='upper left', shadow=True, fontsize='medium')

axarr[1].scatter(hubData[1], vel)
axarr[1].plot(hubData[1], (H * hubData[1]))
axarr[1].set_title('Corrected Velocity')
axarr[1].set_xlabel('Dist (Mpc)')
axarr[1].set_ylabel('Z * c')
axarr[1].legend(['Ho', 'Ho Corrected'], loc='upper left', shadow=True, fontsize='medium')

f.tight_layout()
#pl.savefig('../images/bruder_project4C.png')
pl.show()
