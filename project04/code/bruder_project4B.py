from astropy.io import fits
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import scipy.stats as stats
from bruder_project4A import linfit

#Retrieving data

hubbleData = np.genfromtxt('../data/hubbleoriginal.csv', delimiter=',', dtype=float, skip_header=True)

hubData = hubbleData.T

ned1d = np.genfromtxt('../data/ned1dlevel5.csv', delimiter=',', dtype=float, skip_header=2)

ned1dT = ned1d.T

ned4d = np.genfromtxt('../data/ned4dlevel5.csv', delimiter=',', dtype=float, skip_header=2)

ned4dT = ned4d.T

#eliminating stray data point from near field data

h = ned1dT[3]
j = ned1dT[11]
ned1x = h[np.where(ned1dT[3]<650)]
ned1y = j[np.where(ned1dT[3]<650)]

#converting from km/s to delta lambda over lambda times c to remove relativistic effects

c = 300000
d = (np.sqrt(1 + ned1y / c) / np.sqrt(1 - ned1y / c)) - 1
e = (np.sqrt(1 + ned4dT[10] / c) / np.sqrt(1 - ned4dT[10] / c)) - 1

#abbrieviating far field data to include only galaxies with recession velocities less than 500000

f = ned4dT[3]
n4x = f[np.where((e*c)<500000)]
n4y = e[np.where((e*c)<500000)]

#calculating linear fit to data

A2, A, B, deltaY, deltaA, deltaB, chi2, pvalue = linfit(hubData[1], hubData[2])

A21, A1, B1, deltaY1, deltaA1, deltaB1, chi21, pvalue1 = linfit(ned1x, (d*c))

A24, A4, B4, deltaY4, deltaA4, deltaB4, chi24, pvalue4 = linfit(ned4dT[3], (e*c))

A25, A5, B5, deltaY5, deltaA5, deltaB5, chi25, pvalue5 = linfit(n4x, (n4y*c))

#scatter plot of data

pl.scatter(hubData[1], hubData[2])
pl.scatter(ned1x, (d*c), color='g', marker='.')
pl.scatter(ned4dT[3], (e*c), color='b', marker='.')
pl.ylabel('Recession Velocity')
pl.xlabel('Dist (Mpc)')
pl.title('Combined Data')
pl.savefig('../images/bruder_project4B1.png')
pl.show()

#------------------------------------------------------------------------
#retrieving only SNIa data from far field

sn = np.genfromtxt('../data/ned4dlevel5.csv', delimiter=',', dtype=str, skip_header=2)

sn = sn.T

k = sn[4]
n4xt = f[np.where(k == 'SNIa')]
n4yt = e[np.where(k == 'SNIa')] * c

A2t, A1t, B1t, deltaY1t, deltaA1t, deltaB1t, chi21t, pvalue1t = linfit(n4xt, n4yt)

pl.scatter(n4xt, n4yt)
pl.plot(n4xt, (A1t * n4xt))
pl.show()

#---------------------------------------------------------------------------

xvalues = np.linspace(0, 500)
xvalues2 = np.linspace(0, 10000)

# Four axes, returned as a 2-d array
fig, axarr = pl.subplots(2, 2, figsize = (10, 10))
axarr[0, 0].scatter(ned1x, (d*c), color='g', marker='.')
axarr[0, 0].scatter(ned4dT[3], (e*c), color='b', marker='.')
axarr[0, 0].scatter(hubData[1], hubData[2], color='r', marker='.')
axarr[0, 0].plot(xvalues, (A2 * xvalues), color='r')
axarr[0, 0].plot(ned4dT[3], (A24 * ned4dT[3]), color='b')
axarr[0, 0].plot(xvalues2, (A21 * xvalues2), color='g')
axarr[0, 0].set_ylabel('Z * c')
axarr[0, 0].set_xlabel('Dist (Mpc)')
axarr[0, 0].set_title('Combined')
axarr[0, 0].legend(['Hubble H=424', 'Far Field H=102', 'Near Field H=68'], loc='upper left', shadow=True, fontsize='small')

axarr[0, 1].scatter(ned1x, (d*c), color='g', marker='.')
axarr[0, 1].plot(ned1x, (A21 * ned1x), color='g')
axarr[0, 1].set_ylabel('Z * c')
axarr[0, 1].set_xlabel('Dist (Mpc)')
axarr[0, 1].set_title('Near Field Data')
axarr[0, 1].legend(['Near Field H=68'], loc='upper left', shadow=True, fontsize='medium')

axarr[1, 0].scatter(n4x, (n4y*c), color='b', marker='.')
axarr[1, 0].plot(n4x, (A5 * n4x), color='b')
axarr[1, 0].plot(xvalues2, (A1 * xvalues2), color='g')
axarr[1, 0].set_ylabel('Z * c')
axarr[1, 0].set_xlabel('Dist (Mpc)')
axarr[1, 0].set_title('Far Field Abbrieviated')
axarr[1, 0].legend(['Far Field H=80', 'Near Field H=68'], loc='upper left', shadow=True, fontsize='medium')

axarr[1, 1].scatter(n4xt, n4yt, color='b', marker='.')
axarr[1, 1].plot(n4xt, (A1t * n4xt), color='b')
#axarr[1, 1].plot(xvalues2, (A1 * xvalues2), color='g')
axarr[1, 1].set_ylabel('Z * c')
axarr[1, 1].set_xlabel('Dist (Mpc)')
axarr[1, 1].set_title('SNIa')
axarr[1, 1].legend(['SNIa H=81'], loc='upper left', shadow=True, fontsize='medium')

fig.tight_layout()
pl.savefig('../images/bruder_project4B2.png')
pl.show()

print 'hubble', A2, chi2, pvalue
print 'near galaxies', A21, chi21, pvalue1
print 'far galaxies', A24, chi24, pvalue4
print 'far abbreviated', A25, chi25, pvalue5
print 'SNIa', A2t


#--------------------------------------------

