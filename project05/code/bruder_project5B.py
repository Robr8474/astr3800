from astropy.io import fits
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import scipy.stats as stats
from functions import *

pl.ion()
pl.close()

# Retrieving solar velocity data

data = np.genfromtxt('../data/SUN_Velocity.txt', dtype='float64', skip_header=0)

data = data.T

time =  data[0] - data[0, 0]

# Resetting time array to t0 = 0

steps = []
for i in range(len(time) - 1):
    steps.append(time[i+1] - time[i])

#steps = [steps.append(time[i+1] - time[i] for i in range(len(time) - 1))]

stepsize = np.array(steps)

# Creating new time array of equally spaced steps

max = np.max(stepsize)

time2 = np.arange(0, time[-1], max, dtype='float64') 

dt = time2[2] - time2[1] # delta time

# Calculating interpolated velocities

velnew = np.interp(time2, time, data[1])

# Creating 7 day time slice

time7uni, vel7uni = timeslice(time2, velnew, 11, 18)

# Removing mean and linear trend from data

A, B = linfit(time2, velnew)
A1, B1 = linfit(time7uni, vel7uni)

dataA = velnew - (time2 * A + B)
dataB = vel7uni - (time7uni * A1 + B1)

# Calculating power spectrum 

freqSpec = np.fft.fft(dataA)
freqSpec = freqSpec * np.conj(freqSpec) / len(freqSpec)
freqs = np.fft.fftfreq(freqSpec.size, (dt * 24 *3600))
freqSpec = freqSpec[np.where(freqs >= 0.)]
freqs = freqs[np.where(freqs >= 0.)] * 1000

freqSpec7 = np.fft.fft(dataB)
freqSpec7 = freqSpec7 * np.conj(freqSpec7) / len(freqSpec7)
freqs7 = np.fft.fftfreq(freqSpec7.size, (dt * 24 *3600))
freqSpec7 = freqSpec7[np.where(freqs7 >= 0.)]
freqs7 = freqs7[np.where(freqs7 >= 0.)] * 1000

# Log-log plot of power spectrum  

fig, axarr = pl.subplots(2, figsize = (10, 10))
axarr[0].plot(freqs, freqSpec)
axarr[0].set_xscale('log')
axarr[0].set_yscale('log')
axarr[0].set_xlim(1e-4, 10)
axarr[0].set_ylim(1e-3, 1e6)
axarr[0].set_ylabel('Power (W/m^2)')
axarr[0].set_xlabel('Frequency (mHz)')
axarr[0].set_title('Full Time Period Power Spectrum')
#axarr[0].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

axarr[1].plot(freqs7, freqSpec7)
axarr[1].set_xscale('log')
axarr[1].set_yscale('log')
axarr[1].set_xlim(1e-4, 10)
axarr[1].set_ylim(1e-3, 1e6)
axarr[1].set_ylabel('Power (W/m^2)')
axarr[1].set_xlabel('Frequency (mHz)')
axarr[1].set_title('7 Day Time Period Power Spectrum')
#axarr[1].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

pl.savefig('../images/bruder_project5Bii.png')
#pl.show()

#------------------------------------------------------------------------
pl.close()

# Log-log plot of 2-7 mHz

fig, axarr = pl.subplots(2, figsize = (10, 10))
axarr[0].plot(freqs, freqSpec)
axarr[0].set_xscale('log')
axarr[0].set_yscale('log')
axarr[0].set_xlim(2, 7)
axarr[0].set_ylim(1e-4, 1e3)
axarr[0].set_ylabel('Power (W/m^2)')
axarr[0].set_xlabel('Frequency (mHz)')
axarr[0].set_title('Full Time Period Power Spectrum')
#axarr[0].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

axarr[1].plot(freqs7, freqSpec7)
axarr[1].set_xscale('log')
axarr[1].set_yscale('log')
axarr[1].set_xlim(2, 7)
axarr[1].set_ylim(1e-4, 1e3)
axarr[1].set_ylabel('Power (W/m^2)')
axarr[1].set_xlabel('Frequency (mHz)')
axarr[1].set_title('7 Day Time Period Power Spectrum')
#axarr[1].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

pl.savefig('../images/bruder_project5Biii.png')
#pl.show()

# Plot of 3.2 to 3.4 mHz 

tfull, pfull = timeslice(freqs, freqSpec, 3.2, 3.4)
t2full, p2full = timeslice(freqs, freqSpec, 3.25, 3.35)

t, p = timeslice(freqs7, freqSpec7, 3.2, 3.4)
t2, p2 = timeslice(freqs7, freqSpec7, 3.25, 3.35)

pl.close()

fig, axarr = pl.subplots(2, figsize = (10, 10))
axarr[0].plot(tfull, pfull)
axarr[0].plot(t2full, p2full)
axarr[0].set_yscale('log')
#axarr[0].set_xlim(2, 7)
#axarr[0].set_ylim(1e-4, 1e3)
axarr[0].set_ylabel('Power (W/m^2)')
axarr[0].set_xlabel('Frequency (mHz)')
axarr[0].set_title('Full Time Period Power Spectrum')
#axarr[0].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

axarr[1].plot(t, p)
axarr[1].plot(t2, p2)
#axarr[1].set_xscale('log')
axarr[1].set_yscale('log')
#axarr[1].set_xlim(2, 7)
#axarr[1].set_ylim(1e-4, 1e3)
axarr[1].set_ylabel('Power (W/m^2)')
axarr[1].set_xlabel('Frequency (mHz)')
axarr[1].set_title('7 Day Time Period Power Spectrum')
#axarr[1].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

pl.savefig('../images/bruder_project5Biv.png')
pl.show()

# Chi-squared measure of the spectral peak between 3.285 and 3.315 mHz 

t3full, p3full = timeslice(freqs, freqSpec, 3.285, 3.315)
t3, p3 = timeslice(freqs7, freqSpec7, 3.285, 3.315)

p4full = p2full[np.where(t2full < 3.285)]
p5full = p2full[np.where(t2full > 3.315)]
p4full = np.real(np.append(p4full, p5full))
p3full = np.real(p3full)

p4 = p2[np.where(t2 < 3.285)]
p5 = p2[np.where(t2 > 3.315)]
p4 = np.real(np.append(p4, p5))
p3 = np.real(p3)

obsfull = np.sum((p3full - np.mean(p4full))**2)
chifull = obsfull / np.var(p4full)

pvaluefull = 1 - stats.chi2.cdf(chifull, (len(p3full)-2))

obs = np.sum((p3 - np.mean(p4))**2)
chi = obs / np.var(p4)

pvalue = 1 - stats.chi2.cdf(chi, (len(p3)-2))

print chifull, pvaluefull
print chi, pvalue

# Chi-squared measure of the spectral peakdip between 3.3124 and 3.325 mHz 

t3fulldip, p3fulldip = timeslice(freqs, freqSpec, 3.3124, 3.325)

t3dip, p3dip = timeslice(freqs7, freqSpec7, 3.3124, 3.325)

p3fulldip = np.real(p3fulldip)
p3dip = np.real(p3dip)

obsfulldip = np.sum((p3fulldip - np.mean(p4full))**2)
chifulldip = obsfulldip / np.var(p4full)

pvaluefulldip = 1 - stats.chi2.cdf(chifulldip, (len(p3fulldip)-2))

obsdip = np.sum((p3dip - np.mean(p4))**2)
chidip = obsdip / np.var(p4)

pvaluedip = 1 - stats.chi2.cdf(chidip, (len(p3dip)-2))

print chifulldip, pvaluefulldip
print chidip, pvaluedip

