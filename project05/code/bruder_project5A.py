from astropy.io import fits
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import scipy.stats as stats
from functions import *

pl.ion()
pl.close()

# Retrieving data

data = np.genfromtxt('../data/SUN_Velocity.txt', dtype='float64', skip_header=0)

data = data.T

time =  data[0] - data[0, 0]

# Plot of velocity of solar surface as function of time

pl.plot(time, data[1])
pl.xlabel('Time (days)')
pl.ylabel('Velocity (m/s)')
pl.title('Doppler Velocity of Solar Surface')
pl.show()

pl.savefig('../images/bruder_project5Aii.png')

# Examination of time steps and creation of a uniform time sequence with step size equal to the max interval

steps = []
#for i in range(len(time) - 1):
    #steps.append(time[i+1] - time[i])

stepsize = time[1:] - time[:-1]
#stepsize = np.array(steps)

max = np.max(stepsize)
min = np.min(stepsize)
avg = np.mean(stepsize)
sd = np.std(stepsize)

print max, min, avg, sd

n = (time[-1] / max)

time2 = np.arange(0, time[-1], max, dtype='float64')

velnew = np.interp(time2, time, data[1])

# Creating time slices of 7 days, 6 hrs and 1 hr

time7, vel7 = timeslice(time, data[1], 11, 18)

time7uni, vel7uni = timeslice(time2, velnew, 11, 18)

time6h, vel6h = timeslice(time, data[1], 11, 11.25)
time6huni, vel6huni = timeslice(time2, velnew, 11, 11.25)

time1h, vel1h = timeslice(time, data[1], 11, (11.041667))
time1huni, vel1huni = timeslice(time2, velnew, 11, (11.041667))

# Plot of different time slices

fig, axarr = pl.subplots(2, 2, figsize = (10, 10))
axarr[0, 0].plot(time, data[1], color='b')
axarr[0, 0].plot(time2, velnew, color='g')
axarr[0, 0].set_ylabel('Velocity (m/s)')
axarr[0, 0].set_xlabel('Time (days)')
axarr[0, 0].set_title('Full Time Period')
axarr[0, 0].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

axarr[0, 1].plot(time7, vel7, color='b')
axarr[0, 1].plot(time7uni, vel7uni, color='g')
axarr[0, 1].set_ylabel('Velocity (m/s)')
axarr[0, 1].set_xlabel('Time (days)')
axarr[0, 1].set_title('7 Days')
axarr[0, 1].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

axarr[1, 0].plot(time6h, vel6h, color='b')
axarr[1, 0].plot(time6huni, vel6huni, color='g')
axarr[1, 0].set_ylabel('Velocity (m/s)')
axarr[1, 0].set_xlabel('Time (days)')
axarr[1, 0].set_title('6 Hours')
axarr[1, 0].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

axarr[1, 1].plot(time1h, vel1h, color='b')
axarr[1, 1].plot(time1huni, vel1huni, color='g')
axarr[1, 1].set_ylabel('Velocity (m/s)')
axarr[1, 1].set_xlabel('Time (days)')
axarr[1, 1].set_title('1 Hour')
axarr[1, 1].legend(['Original Time Series', 'Uniform Time Series'], loc='upper right', shadow=True, fontsize='small')

fig.tight_layout()
pl.savefig('../images/bruder_project5Aiv.png')
pl.show()

# Removing linear trends from data

A, B = linfit(time2, velnew)
A1, B1 = linfit(time7uni, vel7uni)
mn = np.mean(vel7uni)

dataA = velnew - (time2 * A + B)
dataB = vel7uni - (time7uni * A1 + B1)

#pl.close()
#pl.plot(time7uni, dataB)
#pl.show()




