from saverestore import *
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm

#creating random star field

seed = 2400
np.random.seed(seed)
numArray1 = np.random.uniform(0, 1, 332)

seed = 3400
np.random.seed(seed)
numArray2 = np.random.uniform(0, 1, 332)

ypos = numArray1 * 2000
xpos = numArray2 * 1500

#retieving data for actual star field

data = np.genfromtxt('../data/frame-g-000094-1-0131.csv', delimiter=',', dtype=float, skip_header=True)

x, y = np.shape(data)

#calculating min distances for real star field

dist = 0
distMin = 10000
minDist = []

for i in range(x):
    for j in range(x):
        if i != j:
            dist = np.sqrt((data[i,0] - data[j,0])**2 + (data[i,1] - data[j,1])**2)
            #print dist
            delta = dist - distMin
            if delta < 0.:
                distMin = dist

    minDist.append(distMin)
    distMin = 10000

#calculating min distances for random star field
    
minDist2 = []

for i in range(len(xpos)):
    for j in range(len(xpos)):
        if i != j:
            dist = np.sqrt((xpos[i] - xpos[j])**2 + (ypos[i] - ypos[j])**2)
            #print dist
            delta = dist - distMin
            if delta < 0.:
                distMin = dist

    minDist2.append(distMin)
    distMin = 10000

binsize = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150]

f, axarr = pl.subplots(2, figsize = (10, 10))
axarr[0].hist(minDist, binsize, normed=0, facecolor='g')
axarr[0].set_title('Actual Star Field')
axarr[0].set_xlabel('Min Distance Between Stars')
axarr[0].set_ylabel('Occurances')

axarr[1].hist(minDist2, binsize, normed=0, facecolor='g')
axarr[1].set_title('Random Star Field')
axarr[1].set_xlabel('Min Distance Between Stars')
axarr[1].set_ylabel('Occurances')
f.tight_layout()
pl.savefig('bruder_project2Bii.png')
pl.show()

sum = 0
sum2 = 0
N = len(minDist2)






