from astropy.io import fits 
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm

#retrieving fits data

data2, hdr  = fits.getdata('../data/frame-g-000094-1-0131.fits', header=True)

#removing negative numbers

x,y = np.shape(data2)
for i in range(x):
    for j in range(y):
        if data2[i,j] < 0:
            data2[i,j] = 0
        else:
            continue

#taking logarithm and scaling of the data

logdata = np.log10(data2 + .025)

pl.imshow(logdata, cmap='gray')
pl.clim(0,.005)
pl.ylim(0, 1500)
pl.xlabel('RA')
pl.ylabel('DEC')
pl.savefig('bruder_project2Ai.png')
pl.show()


