import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm

#creating random star field

seed = 2400
np.random.seed(seed)
numArray1 = np.random.uniform(0, 1, 332)

seed = 3400
np.random.seed(seed)
numArray2 = np.random.uniform(0, 1, 332)

#rescaling to be same as real star field

ypos = numArray1 * 2000
xpos = numArray2 * 1500

pl.scatter(xpos, ypos)
pl.xlim(-200, 1600)
pl.ylim(-500, 2500)
pl.xlabel('RA')
pl.ylabel('DEC')
pl.savefig('bruder_project2Bi.png')
pl.show()

#from saverestore import *

#save("../data/bruder_project2Bi.dat", ('starArray'), (starArray))
