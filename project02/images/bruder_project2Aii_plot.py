from astropy.io import fits 
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm

#retrieving data

data = np.genfromtxt('../data/frame-g-000094-1-0131.csv', delimiter=',', dtype=float, skip_header=True)

#making scatter plot of data

x, y = np.shape(data)

RA=[]
Dec=[]

for i in range(x):
    
        RA.append(data[i,0])
        Dec.append(data[i,1])

pl.scatter(RA, Dec)
pl.xlabel('RA')
pl.ylabel('DEC')
pl.savefig('bruder_project2Aii.png')
pl.show()
