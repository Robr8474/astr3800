from saverestore import *
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm

#create random star field

seed = 2400
np.random.seed(seed)
numArray1 = np.random.uniform(0, 1, 332)

seed = 3400
np.random.seed(seed)
numArray2 = np.random.uniform(0, 1, 332)

ypos = numArray1 * 2000
xpos = numArray2 * 1500

#import real star field data

data = np.genfromtxt('../data/frame-g-000094-1-0131.csv', delimiter=',', dtype=float, skip_header=True)


#-----------------------------------------------------------------------

def freqTest(values1, values2, bins):

    """
    Calculate chi squared for an array of uniformly distributed randum numbers.

    Input Parameters
    ----------
    values1 : numpy array of int or float
        array of randum numbers
    values2 : numpy array of int or float
        array of randum numbers
    bins : list of int, float
        list of bin intervals
    events : int, float
        number of events

    Example
    -------
    To compute chi squared for an array of 1000 numbers using 10 bins:
    >>> chi2 = freqTest(xArray, 10, 1000)

    Returns
    -------
    chi squared : float
    """

    n1Array, bin1Array, patch1Array = pl.hist(values1, bins, normed=0, facecolor='g')
    #print n1Array
    pl.clf()

    n2Array, bin2Array, patch2Array = pl.hist(values2, bins, normed=0, facecolor='g')
    
    pl.clf()
       
    chi2 = np.sum((n1Array - n2Array)**2 / n2Array)

    chi22 = stats.chisquare(n1Array, n2Array)

    #n1Array3 = n1Array[1:]
    #n2Array3 = n2Array[1:]

    #chi23 = stats.chisquare(n1Array3, n2Array3)

    return chi2, chi22

#calculating min distances for the real star field

x, y = np.shape(data)
dist = 0
distMin = 10000
minDist = []

for i in range(x):
    for j in range(x):
        if i != j:
            dist = np.sqrt((data[i,0] - data[j,0])**2 + (data[i,1] - data[j,1])**2)
            
            delta = dist - distMin
            if delta < 0.:
                distMin = dist

    minDist.append(distMin)
    distMin = 10000

#calculating min distances for the random star field

minDist2 = []

for i in range(len(xpos)):
    for j in range(len(xpos)):
        if i != j:
            dist = np.sqrt((xpos[i] - xpos[j])**2 + (ypos[i] - ypos[j])**2)
            #print dist
            delta = dist - distMin
            if delta < 0.:
                distMin = dist

    minDist2.append(distMin)
    distMin = 10000

# Calculating chi-squared

bins = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150]
#How well does random star field simulate a real star field

print 'Chi-squared computation of real star field as expected'
print freqTest(minDist2, minDist, bins)

#How close is a real star field to random

print 'Chi-squared computation of random star field as expected'
print freqTest(minDist, minDist2, bins)




