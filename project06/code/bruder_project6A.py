from astropy.io import fits
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import scipy.stats as stats
from functions import *

pl.ion()
pl.close()

# Retrieving data

data = np.genfromtxt('../data/wasp_4b.tsv', dtype='float64', comments='#', delimiter=';', skip_header=36)

dataT = data.T

# resetting data to start at 0

time = dataT[0] - dataT[0, 0]

# creating plot of transits in time series

pl.plot(time, dataT[1])
pl.xlabel('Time (days)')
pl.ylabel('Flux (W/m^2)')
pl.title('Uncut Transit Image')
pl.savefig('../images/bruder_project6Ai.png')
pl.show()
pl.close()

# cutting up time series into individual transits using looking for large gaps in the time series

transits = []
flux = []
error = []
timelist = []
fluxlist = []
errorlist = []

for i in range(len(time)-1):
    timelist.append(time[i])
    fluxlist.append(dataT[1,i])
    errorlist.append(dataT[2,i])
    int = time[i+1] - time[i]
    
    if int > 2.4:
        timelist = np.array(timelist)
        transits.append((timelist - timelist[0]) * 24)
        flux.append(fluxlist)
        error.append(errorlist)
        timelist = []
        fluxlist = []
        errorlist = []
        
transits.append((timelist - timelist[0]) * 24)
flux.append(fluxlist)
error.append(errorlist)

# creating arrays of the transit data

transits = np.array(transits) 
flux = np.array(flux)
error = np.array(error)

t1, t2, t3, t4, t5, t6 = transits
f1, f2, f3, f4, f5, f6 = flux
e1, e2, e3, e4, e5, e6 = error

# Plot of individual transits

fig, axarr = pl.subplots(2, 3, figsize = (10, 10))

e = 0

for i in range(2):
    for j in range(3):
    
        axarr[i, j].plot(transits[e], flux[e], color='b')
        axarr[i, j].set_ylabel('Flux (W/m^2)')
        axarr[i, j].set_xlabel('Time (days)')
        axarr[i, j].set_title('Transit %s' %e)

        e += 1
    
fig.tight_layout()
pl.savefig('../images/bruder_project6Aii.png')
pl.show()
pl.close()

# composite plot of individual transits

for i in range(len(transits)):
    pl.plot(transits[i], flux[i])

pl.xlabel('Time (days)')
pl.ylabel('flux (W/m^2)')
pl.title('Transit Composite')
#pl.xlim(.3, 3.8)
pl.legend(['Transit 1', 'Transit 2','Transit 3','Transit 4','Transit 5','Transit 6',], loc='lower right', shadow=True, fontsize='small')
pl.savefig('../images/bruder_project6Aiv.png')
pl.show()
pl.close()

#save("../data/bruder_project6A.dat", ('transit', '1', '2', '3', '4', '5'), (transits))
#save("../data/bruder_project6Aflux.dat", ('flux', '1', '2', '3', '4', '5'), (flux))
#save("../data/bruder_project6Aerror.dat", ('error'), (error))

# could not get save/restore to work and since the program is not computationally intensive, just continued to work in this file

#-----------------------------Part B--------------------------------------------

# creating a uniformly spaced time series based on transit 2

steps = []

steps.append([t2[i+1] - t2[i] for i in range(len(t2) - 1)])

stepsize = np.array(steps)
max = np.max(stepsize)

t2int = np.arange(0, t2[-1], max, dtype='float64') # uniform time series

dt = t2int[2] - t2int[1] # delta time

# Calculating interpolated velocities

f1int = np.interp(t2int, t1, f1)
f2int = np.interp(t2int, t2, f2)
f3int = np.interp(t2int, t3, f3)
f4int = np.interp(t2int, t4, f4)
f5int = np.interp(t2int, t5, f5)
f6int = np.interp(t2int, t6, f6)

# creating transit arrays of equal length (redundant)

t1a, f1int = timeslice(t2int, f1int, 0, 3.7)
t2a, f2int = timeslice(t2int, f2int, 0, 3.7)
t3a, f3int = timeslice(t2int, f3int, 0, 3.7)
t4a, f4int = timeslice(t2int, f4int, 0, 3.7)
t5a, f5int = timeslice(t2int, f5int, 0, 3.7)
t6a, f6int = timeslice(t2int, f6int, 0, 3.7)

# calculating the timeshift that maximizes the cross-correlation of each transit with the reference (transit 2)

times = np.array([t1a, t2a, t3a, t4a, t5a, t6a])
fluxes = np.array([f1int, f2int, f3int, f4int, f5int, f6int])

# cross-correlation in temporal space

maxcorr = []

tsize = np.size(f2int)
crosscor = np.zeros(tsize)
for i in range(len(fluxes)):
    for shift in range(0, tsize):

        crosscor[shift] = np.sum(fluxes[1] * np.roll(fluxes[i], shift))
        
    maxcorr.append(crosscor.argmax())

print maxcorr

# shifting the time series

t = []
tcorr = []
for i in range(len(times)):
    t = (times[i] + maxcorr[i] * dt) - np.max(times[1])
    tcorr.append(t)
    t = []
tcorr[1] = tcorr[1] + np.max(times[1])

# cross-correlation in temporal space

fftcorr = []

for i in range(len(fluxes)):
    
    ccorr = np.fft.ifft(np.fft.fft(fluxes[1,:]) * np.conj(np.fft.fft(fluxes[i,:])))
    fftcorr.append(ccorr.argmax())
    ccorr = []
        
print fftcorr

# plot of the cross-correlated transits

pl.close()
for i in range(len(fluxes)):
    pl.plot(tcorr[i], fluxes[i], '^')

pl.xlabel('Time (days)')
pl.ylabel('Flux (W/m^2)')
pl.title('Time Shifted Transit Composite') 
#pl.ylim(.970, .975)
#pl.xlim(1.6, 1.9)
pl.legend(['Transit 1', 'Transit 2','Transit 3','Transit 4','Transit 5','Transit 6',], loc='lower left', shadow=True, fontsize='small')
pl.savefig('../images/bruder_project6Bii.png')
pl.show()
pl.close()

#------------------------------Part C-----------------------------------------

# calculating the non-transit flux using a timeslice that contains the non-transit flux of all  6 transits

c = 0
d = 0.4

t1max, f1max = timeslice(tcorr[0], f1int, c, d)
t2max, f2max = timeslice(tcorr[1], f2int, c, d)
t3max, f3max = timeslice(tcorr[2], f3int, c, d)
t4max, f4max = timeslice(tcorr[3], f4int, c, d)
t5max, f5max = timeslice(tcorr[4], f5int, c, d)
t6max, f6max = timeslice(tcorr[5], f6int, c, d)

fmax = np.array([f1max, f2max, f3max, f4max, f5max, f6max])

ntflux = (np.mean(fmax)) # mean value of non-transit flux
deltamax = np.std(fmax) / (np.sqrt(len(f2max) - 1)) # SDOM of non-transit flux

# computing depth of transit by computing centerthe mid-point of the drop and rise then varying the width of the sample used centered on the mid-point to compute the depth

center = np.where((f2int<.991) & (f2int>.989))

a = t2[130] - .03
b = t2[130] + .03

mean = [] # width of the sample
std = [] # standard deviation of the sample
depth = [] # depth of the transit
rPlanet = [] # radius of WASP-4b
Rsun = 6.955e5 # radius of sun (km)
Rstar = 0.87 * Rsun # radius of WASP-4

for step in range(22):
    
    t1min, f1min = timeslice(tcorr[0], f1int, a, b)
    t2min, f2min = timeslice(tcorr[1], f2int, a, b)
    t3min, f3min = timeslice(tcorr[2], f3int, a, b)
    t4min, f4min = timeslice(tcorr[3], f4int, a, b)
    t5min, f5min = timeslice(tcorr[4], f5int, a, b)
    t6min, f6min = timeslice(tcorr[5], f6int, a, b)

    fmin = np.array([f1min, f2min, f3min, f4min, f5min, f6min])
    mean.append(b-a)

    std.append(np.std(fmin))
    depth.append(np.mean(fmax) - np.mean(fmin))

    rPlanet.append(np.sqrt(Rstar**2 * (np.mean(fmax) - np.mean(fmin)) / np.mean(fmax)))

    a -= step * 0.005
    b += step * 0.005

# plot of rPlanet as a function of the width of the sample used to compute depth of transit
    
pl.plot(mean, rPlanet)
pl.xlabel('Width of Min Flux')
pl.ylabel('Radius (km)')
pl.title('Radius of WASP-4b')
pl.savefig('../images/bruder_project6Cii.png')
pl.show()
#pl.close()

# choosing a depth and radius where the standard deviation of the flux at the depth of the transit is minimum
 
deptht = depth[np.argmin(std)]
Rplanet = rPlanet[np.argmin(std)]

# computing uncertainty in ratio and radius of WASP-4b

deltamin = np.max(std) / np.sqrt(43) # uncertainty in depth (44 = # of data points to compute the depth)
                  
ratio = deptht / ntflux 

deltaratio = np.sqrt(((deltamin / ntflux)**2) + ((deltamax * (deptht / ntflux**2))**2))

deltaStar = 0.04 * Rsun # uncertainty in radius of WASP-4

deltaradius  = np.sqrt(((0.5 * Rstar**2 / (np.sqrt(ratio * Rstar**2)) * deltaratio)**2) + ((Rstar * ratio / (np.sqrt(ratio * Rstar**2)) * deltaStar)**2))

Rjupiter = 71492 # (km)

print Rplanet, 1.304 * Rjupiter, deltaradius

# computing density of WASP-4b and uncertainty
 
Mjupiter = 1.8986e27 # mass of jupiter (kg)
Mplanet = 1.21 * Mjupiter # mass of WASP-4b
deltaM = 0.12 * Mjupiter # uncertainty in mass of WASP-4b

dplanet = Mplanet / (4/3 * np.pi * (Rplanet * 1000.)**3) * (1000./100.**3)

deltadplanet  = np.sqrt(((deltaM / (4/3  * np.pi * (Rplanet * 1000.)**3) * (1000./100.**3))**2) + ((3 * Mplanet / (4/3 * np.pi * (Rplanet * 1000.)**4) * (deltaradius * 1000) * (1000./100.**3))**2))

print dplanet, deltadplanet
    
#-------------------------Thought question--------------------------------------

avg = (fluxes[1] + fluxes[2]) / 2

#pl.close()
#pl.plot(tcorr[1], avg)
#pl.show()


#radius  = np.sqrt(((0.5 * Rstar**2 / (np.sqrt(ratio * Rstar**2)) * deltaratio)**2)
