import numpy
import matplotlib.pylab as pl
from saverestore import *

#-------------------Frequency test for human generated numbers-------------------------



def freqTest(values, bins=10, events=1000):

    """
    Calculate chi squared for an array of uniformly distributed randum numbers.

    Input Parameters
    ----------
    values : numpy array of int or float
        array of randum numbers
    bins : int, float
        number of bins
    events : int, float
        number of events

    Example
    -------
    To compute chi squared for an array of 1000 numbers using 10 bins:
    >>> chi2 = freqTest(xArray, 10, 1000)

    Returns
    -------
    chi squared : float
    """
    
    bins = float(bins)

    nArray, binArray, patchArray = pl.hist(values, bins=bins, normed=0, facecolor='g')
    pl.clf()
       
    chi2 = 0

    for i in range(len(nArray)):
    
        chi2 += ((nArray[i] / events) - (1 / bins))**2 / (1 / bins)
    
    return chi2

x, y, z = restore('../data/bruder_project1Ai.dat')

print 'Human generated trial 01, 10 bins, chi squared = %f.5' %freqTest(x,10, 1000)
print 'Human generated trial 02, 10 bins, chi squared = %f.5' %freqTest(y,10, 1000)
print 'Human generated trial 03, 10 bins, chi squared = %f.5' %freqTest(z,10, 1000)
print 'Human generated trial 01, 40 bins, chi squared = %f.5' %freqTest(x,40, 1000)
print 'Human generated trial 02, 40 bins, chi squared = %f.5' %freqTest(y,40, 1000)
print 'Human generated trial 03, 40 bins, chi squared = %f.5' %freqTest(z,40, 1000)
print 'Human generated trial 01, 100 bins, chi squared = %f.5' %freqTest(x,100, 1000)
print 'Human generated trial 02, 100 bins, chi squared = %f.5' %freqTest(y,100, 1000)
print 'Human generated trial 03, 100 bins, chi squared = %f.5' %freqTest(z,100, 1000)

#combining values from all 3 human generated events
c = x
c1 = np.append(c, y)
c2 = np.append(c1, z)

print 'Human generated trials combined, 10 bins, chi squared = %f.5' %freqTest(c2,10, 3000)
print 'Human generated trials combined, 40 bins, chi squared = %f.5' %freqTest(c2,40, 3000)
print 'Human generated trials combined, 100 bins, chi squared = %f.5' %freqTest(c2,100, 3000)


#---------------Frequency test for Computer generated numbers-----------------------

x, y, z = restore('../data/bruder_project1Aii.dat')
                    
print 'Computer generated trial 01, 10 bins, chi squared = %f.5' %freqTest(x, 10, 1000)
print 'Computer generated trial 01, 40 bins, chi squared = %f.5' %freqTest(x, 40, 1000)
print 'Computer generated trial 01, 100 bins, chi squared = %f.5' %freqTest(x, 100, 1000)
print 'Computer generated trial 02, 10 bins, chi squared = %f.5' %freqTest(y, 10, 1000)
print 'Computer generated trial 02, 40 bins, chi squared = %f.5' %freqTest(y, 40, 1000)
print 'Computer generated trial 02, 100 bins, chi squared = %f.5' %freqTest(y, 100, 1000)
print 'Computer generated trial 03, 10 bins, chi squared = %f.5' %freqTest(z, 10, 1000)
print 'Computer generated trial 03, 40 bins, chi squared = %f.5' %freqTest(z, 40, 1000)
print 'Computer generated trial 03, 100 bins, chi squared = %f.5' %freqTest(z, 100, 1000)

#combining values from all 3 computer generated events
c = x
c1 = np.append(c, y)
c2 = np.append(c1, z)

print 'Computer generated trials combined, 10 bins, chi squared = %f.5' %freqTest(c2,10, 3000)
print 'Computer generated trials combined, 40 bins, chi squared = %f.5' %freqTest(c2,40, 3000)
print 'Computer generated trials combined, 100 bins, chi squared = %f.5' %freqTest(c2,100, 3000)


#--------------Serial Test--------------------------------------------------

def serialtest(values):

    """
    Calculate chi squared for a serial test of an array of uniformly distributed randum numbers on the interval [0'1).

    Input Parameters
    ----------
    values : numpy array of int or float
        array of randum numbers
    
    Example
    -------
    To compute chi squared for an array of 1000 numbers:
    >>> chi2 = serialtest(xArray)

    Returns
    -------
    chi squared : float
    """
    
    x = np.round(values, 1)
    
    xs = x[1: ] - x[: -1]
    
    xs = np.absolute(xs)
    

    xs = xs == .1
      
    nsArray, binsArray, patchsArray = pl.hist(xs, bins=2, normed=0, facecolor='g')

           
    chi2 = 0
    
    chi2 = ((nsArray[1] / 999) - (.18))**2 / (.18) + ((nsArray[0] / 999) - (.82))**2 / (.82)
    
    return chi2

x, y, z = restore('../data/bruder_project1Ai.dat')

print 'human generated trial 01, serial test, chi squared = %f.5' %serialtest(x)
print 'human generated trial 02, serial test, chi squared = %f.5' %serialtest(y)
print 'human generated trial 03, serial test, chi squared = %f.5' %serialtest(z)

x, y, z = restore('../data/bruder_project1Aii.dat')

print 'Computer generated trial 01, serial test, chi squared = %f.5' %serialtest(x)
print 'Computer generated trial 02, serial test, chi squared = %f.5' %serialtest(y)
print 'Computer generated trial 03, serial test, chi squared = %f.5' %serialtest(z)

#-----------------------------------------------------------------------

