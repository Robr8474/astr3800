import numpy as np
import matplotlib.pylab as pl
from saverestore import *


x, y, z = restore('../data/bruder_project1Ai.dat')
x1, y1, z1 = restore('../data/bruder_project1Aii.dat')

hc = x
hc1 = np.append(hc, y)
hc2 = np.append(hc1, z)

c = x1
c1 = np.append(c, y1)
c2 = np.append(c1, z1)

pl.clf()

# Four axes, returned as a 2-d array
f, axarr = pl.subplots(4, 4, figsize = (20, 20))
axarr[0, 0].hist(x, bins=10, normed=0, facecolor='g')
axarr[0, 0].set_title('Human Generated 01')
axarr[0, 0].set_xlabel('value')
axarr[0, 0].set_ylabel('Occurances')

axarr[0, 1].hist(y, bins=10, normed=0, facecolor='g')
axarr[0, 1].set_title('Human Generated 02')
axarr[0, 1].set_xlabel('value')
axarr[0, 1].set_ylabel('Occurances')

axarr[0, 2].hist(z, bins=10, normed=0, facecolor='g')
axarr[0, 2].set_title('Human Generated 03')
axarr[0, 2].set_xlabel('value')
axarr[0, 2].set_ylabel('Occurances')

axarr[0, 3].hist(hc2, bins=10, normed=0, facecolor='g')
axarr[0, 3].set_title('Human Generated Combined')
axarr[0, 3].set_xlabel('value')
axarr[0, 3].set_ylabel('Occurances')

axarr[1, 0].hist(x, bins=40, normed=0, facecolor='g')
axarr[1, 0].set_title('Human Generated 01')
axarr[1, 0].set_xlabel('value')
axarr[1, 0].set_ylabel('Occurances')

axarr[1, 1].hist(y, bins=40, normed=0, facecolor='g')
axarr[1, 1].set_title('Human Generated 02')
axarr[1, 1].set_xlabel('value')
axarr[1, 1].set_ylabel('Occurances')

axarr[1, 2].hist(z, bins=40, normed=0, facecolor='g')
axarr[1, 2].set_title('Human Generated 03')
axarr[1, 2].set_xlabel('value')
axarr[1, 2].set_ylabel('Occurances')

axarr[1, 3].hist(hc2, bins=40, normed=0, facecolor='g')
axarr[1, 3].set_title('Human Generated 03')
axarr[1, 3].set_xlabel('value')
axarr[1, 3].set_ylabel('Occurances')

axarr[2, 0].hist(x1, bins=10, normed=0, facecolor='g')
axarr[2, 0].set_title('Computer Generated 01')
axarr[2, 0].set_xlabel('value')
axarr[2, 0].set_ylabel('Occurances')

axarr[2, 1].hist(y1, bins=10, normed=0, facecolor='g')
axarr[2, 1].set_title('Computer Generated 02')
axarr[2, 1].set_xlabel('value')
axarr[2, 1].set_ylabel('Occurances')

axarr[2, 2].hist(z1, bins=10, normed=0, facecolor='g')
axarr[2, 2].set_title('Computer Generated 03')
axarr[2, 2].set_xlabel('value')
axarr[2, 2].set_ylabel('Occurances')

axarr[2, 3].hist(c2, bins=10, normed=0, facecolor='g')
axarr[2, 3].set_title('Computer Generated Combined')
axarr[2, 3].set_xlabel('value')
axarr[2, 3].set_ylabel('Occurances')

axarr[3, 0].hist(x1, bins=40, normed=0, facecolor='g')
axarr[3, 0].set_title('Computer Generated 01')
axarr[3, 0].set_xlabel('value')
axarr[3, 0].set_ylabel('Occurances')

axarr[3, 1].hist(y1, bins=40, normed=0, facecolor='g')
axarr[3, 1].set_title('Computer Generated 02')
axarr[3, 1].set_xlabel('value')
axarr[3, 1].set_ylabel('Occurances')

axarr[3, 2].hist(z1, bins=40, normed=0, facecolor='g')
axarr[3, 2].set_title('Computer Generated 03')
axarr[3, 2].set_xlabel('value')
axarr[3, 2].set_ylabel('Occurances')

axarr[3, 3].hist(c2, bins=40, normed=0, facecolor='g')
axarr[3, 3].set_title('Computer Generated 03')
axarr[3, 3].set_xlabel('value')
axarr[3, 3].set_ylabel('Occurances')


pl.savefig('bruder_project1Bii.png')
pl.show()


nArray, binArray, patchArray = pl.hist(x, bins=10, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Run 01')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.show()

nArray, binArray, patchArray = pl.hist(y, bins=10, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Run 02')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.show()

nArray, binArray, patchArray = pl.hist(z, bins=10, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Run 03')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.show()

nArray, binArray, patchArray = pl.hist(x, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Run 01')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.show()

nArray, binArray, patchArray = pl.hist(y, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Run 02')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.show()

nArray, binArray, patchArray = pl.hist(z, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Run 03')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.show()

c = x
c1 = np.append(c, y)
c2 = np.append(c1, z)
print len(c2)

nArray, binArray, patchArray = pl.hist(c2, bins=10, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Combined')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurance')
pl.grid(True)
#pl.show()

nArray, binArray, patchArray = pl.hist(c2, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Computer Generated Random Numbers Combined')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurance')
pl.grid(True)
#pl.show()

nArray, binArray, patchArray = pl.hist(x, bins=10, normed=0, facecolor='g')

pl.title('Human Generated Random Numbers Run 01')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.savefig('bruder_project1Bi.png')
#pl.show()
pl.clf()

nArray, binArray, patchArray = pl.hist(y, bins=10, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Human Generated Random Numbers Run 02')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.savefig('bruder_project1Bi.png')
#pl.show()
pl.clf()

nArray, binArray, patchArray = pl.hist(z, bins=10, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Human Generated Random Numbers Run 03')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.savefig('bruder_project1Bi.png')
#pl.show()
pl.clf()

nArray, binArray, patchArray = pl.hist(x, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Human Generated Random Numbers Run 01')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.savefig('bruder_project1Bi.png')
#pl.show()
pl.clf()

nArray, binArray, patchArray = pl.hist(y, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Human Generated Random Numbers Run 02')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.savefig('bruder_project1Bi.png')
#pl.show()
pl.clf()

nArray, binArray, patchArray = pl.hist(z, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Human Generated Random Numbers Run 03')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.savefig('bruder_project1Bi.png')
#pl.show()
pl.clf()




c = x
c1 = np.append(c, y)
c2 = np.append(c1, z)
print len(c2)

nArray, binArray, patchArray = pl.hist(c2, bins=10, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Human Generated Random Numbers Combined')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.savefig('bruder_project1Bi.png')
#pl.show()
pl.clf()

nArray, binArray, patchArray = pl.hist(c2, bins=40, normed=0, facecolor='g')

# Label the plot and add a grid for visualization:
pl.title('Human Generated Random Numbers Combined')
pl.xlabel('Random Number')
pl.ylabel('Number of Occurances')
pl.grid(True)
#pl.show()

#pl.savefig('bruder_project1Bi.png')
pl.clf()


