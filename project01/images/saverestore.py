import numpy as np

def save(filename, names, data):
    '''

    '''

    fyl = open(filename, 'wb')

    for i, name in enumerate(names):
        fyl.write(name + '\n')

        var = data[i]
        shape = var.shape
        shape = ','.join(np.array(shape, dtype=str))
        fyl.write(shape + '\n')

        dtype = str(var.dtype)
        fyl.write(dtype + '\n')

        var_str = var.flatten().tobytes()
        fyl.write(var_str + '\n\n')

    fyl.close()

def restore(filename):
    '''

    '''

    fyl = open(filename, 'rb')

    data = []
    
    while True:

        print ('restoring variables: \n')

        var_name = fyl.readline()
        if var_name == '': break
        print var_name

        shape = fyl.readline().replace('\n','')
        shape = shape.split(',')
        shape = np.array(shape, dtype=int)

        dtype = fyl.readline().replace('\n','')

        data_str = ''
        line = ''
        while line != '\n':
            data_str += line
            line = fyl.readline()

        array = np.fromstring(data_str[:-1], dtype=dtype)
        array = array.reshape(shape)
        data.append(array)

    fyl.close()
    return data

        







    

