import numpy as np
import matplotlib.pyplot as pl
from saverestore import *

x, y, z = restore('../data/bruder_project1Ai.dat')
x1, y1, z1 = restore('../data/bruder_project1Aii.dat')

# Four axes, returned as a 2-d array
f, axarr = pl.subplots(2, 3, figsize = (20, 10))
axarr[0, 0].plot(x, marker = '.', color = 'b', lw = .5)
axarr[0, 0].set_title('a)Human Generated Run 01')
axarr[0, 0].set_xlabel('event')
axarr[0, 0].set_ylabel('value')

axarr[0, 1].plot(y, marker = '.', color = 'b', lw = .5)
axarr[0, 1].set_title('b)Human Generated 02')
axarr[0, 1].set_xlabel('event')
axarr[0, 1].set_ylabel('value')

axarr[0, 2].plot(z, marker = '.', color = 'b', lw = .5)
axarr[0, 2].set_title('c)Human Generated 03')
axarr[0, 2].set_xlabel('event')
axarr[0, 2].set_ylabel('value')

axarr[1, 0].plot(x1, marker = '.', color = 'b', lw = .5)
axarr[1, 0].set_title('d)Computer Generated Run 01')
axarr[1, 0].set_xlabel('event')
axarr[1, 0].set_ylabel('value')

axarr[1, 1].plot(y1, marker = '.', color = 'b', lw = .5)
axarr[1, 1].set_title('e)Computer Generated 02')
axarr[1, 1].set_xlabel('event')
axarr[1, 1].set_ylabel('value')

axarr[1, 2].plot(z1, marker = '.', color = 'b', lw = .5)
axarr[1, 2].set_title('f)Computer Generated 03')
axarr[1, 2].set_xlabel('event')
axarr[1, 2].set_ylabel('value')

pl.savefig('bruder_project1Bi.png')

#-------------------------end of code----------------------------------------


# to do multiple plots in the same figure: http://matplotlib.org/examples/pylab_examples/subplots_demo.html


