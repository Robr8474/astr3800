import numpy as np
import matplotlib.pyplot as pl
from saverestore import *

# Retrieving mean and variance data

mArray, m1Array, mArray2, m1Array2 = restore("../data/bruder_project3Cii.dat")
    
# Retrieving header information

juldate, avgWK, RMSAK, avgWR, RMSAR = restore('../data/bruder_project3Bi.dat')

juldate = (juldate - (int(juldate[0]))) * 24 + 2

# Plotting first and second moments as function of average width of gaussian fit limbs

f, axarr = pl.subplots(2, 2, figsize = (10, 10))
axarr[0,0].plot(avgWK, mArray, 'bo')
axarr[0,0].set_title('Ca Filter Mean Intensity')
axarr[0,0].set_xlabel('Average Width')
axarr[0,0].set_ylabel('Mean')

axarr[0,1].plot(avgWK, m1Array, 'bo')
axarr[0,1].set_title('Ca Filter Standard Deviation')
axarr[0,1].set_xlabel('Average Width')
axarr[0,1].set_ylabel('Standard Deviation')

axarr[1,0].plot(avgWR, mArray2, 'bo')
axarr[1,0].set_title('Red Filter Mean Intensity')
axarr[1,0].set_xlabel('Average Width')
axarr[1,0].set_ylabel('Mean')
#axarr[1,0].set_ylim(-.00042, -.00056)

axarr[1,1].plot(avgWR, m1Array2, 'bo')
axarr[1,1].set_title('Red Filter Standard Deviation')
axarr[1,1].set_xlabel('Average Width')
axarr[1,1].set_ylabel('Standard Deviation')

f.tight_layout()
pl.savefig('bruder_project3Ciii.png')
pl.show()
