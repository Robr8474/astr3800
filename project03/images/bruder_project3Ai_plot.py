from astropy.io import fits
import os
#import fimatch
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm

#retrieving fits data

data1, hdr1  = fits.getdata('../data/20050702.1702.HW.R.P.rdc.fits.gz', header=True)
data2, hdr2  = fits.getdata('../data/20050702.1702.HW.R.P.contrast.fits.gz', header=True)

x, y = np.shape(data1)
x2, y2 =  np.shape(data2)

horCut1 = data1[x / 2]
horCut2 = data2[x2 / 2]

# Four axes, returned as a 2-d array
fig, axarr = pl.subplots(2, 2, figsize = (10, 10))
axarr[0, 0].imshow(data1, cmap='gray')
axarr[0, 0].set_title('Red Continuum rdc image')
axarr[0, 0].set_xlabel('x-Pixel')
axarr[0, 0].set_ylabel('y-Pixel')

axarr[0, 1].imshow(data2, cmap='gray')
axarr[0, 1].set_title('Red Continuum Contrast image')
axarr[0, 1].set_xlabel('x-Pixel')
axarr[0, 1].set_ylabel('y-Pixel')

axarr[1, 0].plot(horCut1)
axarr[1, 0].set_title('Horizontal Cut Through Image Center')
axarr[1, 0].set_xlim(0, 2000)
axarr[1, 0].set_xlabel('x-Pixel')
axarr[1, 0].set_ylabel('Intensity')

axarr[1, 1].plot(horCut2)
axarr[1, 1].set_title('Horizontal Cut Through Image Center')
axarr[1, 1].set_xlim(0, 2000)
axarr[1, 1].set_xlabel('x-Pixel')
axarr[1, 1].set_ylabel('Intensity')

fig.tight_layout()
pl.savefig('bruder_project3Ai.png')
pl.show()


