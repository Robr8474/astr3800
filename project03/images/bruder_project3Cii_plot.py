import numpy as np
import matplotlib.pyplot as pl
from saverestore import *

# Retrieving mean and variance data

mArray, m1Array, mArray2, m1Array2 = restore("../data/bruder_project3Cii.dat")
    
# Retrieving header information

juldate, avgWK, RMSAK, avgWR, RMSAR = restore('../data/bruder_project3Bi.dat')

juldate = (juldate - (int(juldate[0]))) * 24 + 2

# # Plotting first and second moments as function of time of day

f, axarr = pl.subplots(2, 2, figsize = (10, 10))
axarr[0,0].plot(juldate, mArray, marker = '.', color = 'b', lw = .5)
axarr[0,0].set_title('Ca Filter Mean Intensity')
axarr[0,0].set_xlabel('Local Hawaii Time')
axarr[0,0].set_ylabel('Mean')

axarr[0,1].plot(juldate, m1Array, marker = '.', color = 'r', lw = .5)
axarr[0,1].set_title('Ca Filter Standard Deviation')
axarr[0,1].set_xlabel('Local Hawaii Time')
axarr[0,1].set_ylabel('Std Deviation')

axarr[1,0].plot(juldate, mArray2, marker = '.', color = 'b', lw = .5)
axarr[1,0].set_title('Red Filter Mean Intensity')
axarr[1,0].set_xlabel('Local Hawaii Time')
axarr[1,0].set_ylabel('Mean')
#axarr[1,0].set_ylim(-.00042, -.00056)

axarr[1,1].plot(juldate, m1Array2, marker = '.', color = 'r', lw = .5)
axarr[1,1].set_title('Red Filter Standard Deviation')
axarr[1,1].set_xlabel('Local Hawaii Time')
axarr[1,1].set_ylabel('Variance')

f.tight_layout()
pl.savefig('bruder_project3Cii.png')
pl.show()
