import numpy as np
import matplotlib.pyplot as pl
from saverestore import *

# Retrieving PDF data

n1Array, bin1Array, n2Array, bin2Array, n3Array, bin3Array, n4Array, bin4Array, n5Array, bin5Array, n6Array, bin6Array = restore("../data/bruder_project3Ci.dat")

# Plotting PDF's
   
f, axarr = pl.subplots(2, figsize = (10, 10))
axarr[0].plot(bin1Array[0:-1], n1Array, color='r')
axarr[0].plot(bin2Array[0:-1], n2Array, color='g')
axarr[0].plot(bin3Array[0:-1], n3Array, color='b')
axarr[0].set_title('Ca Filter PDF of Contrast Image on Disk')
axarr[0].set_xlabel('Intensity')
axarr[0].set_ylabel('f')
axarr[0].set_xlim(-.1, .2)
axarr[0].legend(['1702z', '1720z', '1740z'], loc=0, shadow=True, fontsize='medium')

axarr[1].plot(bin4Array[0:-1], n4Array, color='r')
axarr[1].plot(bin5Array[0:-1], n5Array, color='g')
axarr[1].plot(bin6Array[0:-1], n6Array, color='b')
axarr[1].set_title('Red Filter PDF of Contrast Image on Disk')
axarr[1].set_xlabel('Intensity')
axarr[1].set_ylabel('f')
axarr[1].set_xlim(-.05, .05)
axarr[1].legend(['1702z', '1720z', '1740z'], loc=0, shadow=True, fontsize='medium')

f.tight_layout()
pl.savefig('bruder_project3Ci.png')
pl.show()
