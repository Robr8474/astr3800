import numpy as np
import matplotlib.pyplot as pl
from saverestore import *

# Retrieving Header data

juldate, avgWK, RMSAK, avgWR, RMSAR = restore('../data/bruder_project3Bi.dat')

juldate = (juldate - (int(juldate[0]))) * 24 + 2

# Plotting Avg Width and RMSA vs time of day

f, axarr = pl.subplots(2, figsize = (10, 10))
axarr[0].plot(juldate, avgWK, marker = '.', color = 'b', lw = .5, label='Ca filter')
axarr[0].plot(juldate, avgWR, marker = '.', color = 'r', lw = .5, label='red filter')
axarr[0].set_title('Average Width as a Function of Time')
axarr[0].set_xlabel('Local Hawaii Time')
axarr[0].set_ylabel('avgW')
axarr[0].legend(['CA filter', 'Red Filter'], loc='upper left', shadow=True, fontsize='medium')

axarr[1].plot(juldate, RMSAK, marker = '.', color = 'b', lw = .5, label='Ca filter')
axarr[1].plot(juldate, RMSAR, marker = '.', color = 'r', lw = .5, label='red filter')
axarr[1].set_title('RMSA value as a Function of Time')
axarr[1].set_xlabel('Local Hawaii Time')
axarr[1].set_ylabel('RMSA')
axarr[1].legend(['CA filter', 'Red Filter'], loc=0, shadow=True, fontsize='medium')

f.tight_layout()
pl.savefig('bruder_project3Bii.png')
pl.show()
