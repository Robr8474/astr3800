from astropy.io import fits
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
from saverestore import *

# Retrieving fits data

KcontFiles = glob('../data/*HW.K.P.contrast*')
RcontFiles = glob('../data/*HW.R.P.contrast*')

# Arranging in chronological order

KcontFiles = np.sort(KcontFiles)
RcontFiles = np.sort(RcontFiles)

files = [KcontFiles, RcontFiles]

# Capturing on limb intensities

int2 = []

for f in KcontFiles:
      
    data = fits.getdata(f)
   
    data1 = data[np.where(data >= -0.3)]
    
    int2.append(data1)
    
int3 = []


for f in RcontFiles:
        
    data2 = fits.getdata(f)
 
    data3 = data2[np.where(data2 >= -0.3)]
    
    int3.append(data3)

# Creating PDF's of the on limb intensities
    
n1Array, bin1Array, patch1Array = pl.hist(int2[1], 1000, normed=1, facecolor='g')

pl.show()
n2Array, bin2Array, patch2Array = pl.hist(int2[2], 1000, normed=1, facecolor='g')

pl.show()
n3Array, bin3Array, patch3Array = pl.hist(int2[3], 1000, normed=1, facecolor='g')

pl.show()

n4Array, bin4Array, patch4Array = pl.hist(int3[1], 1000, normed=1, facecolor='g')
pl.show()

n5Array, bin5Array, patch5Array = pl.hist(int3[2], 1000, normed=1, facecolor='g')
pl.show()

n6Array, bin6Array, patch6Array = pl.hist(int3[3], 1000, normed=1, facecolor='g')
pl.show()

save("../data/bruder_project3Ci.dat", ('n1Array', 'bin1Array', 'n2Array', 'bin2Array', 'n3Array', 'bin3Array', 'n4Array', 'bin4Array', 'n5Array', 'bin5Array', 'n6Array', 'bin6Array'), (n1Array, bin1Array, n2Array, bin2Array, n3Array, bin3Array, n4Array, bin4Array, n5Array, bin5Array, n6Array, bin6Array))

