from astropy.io import fits
import os
import fnmatch
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
from saverestore import *

# Retrieving fits data

KcontFiles = glob('../data/*HW.K.P.contrast*')
RcontFiles = glob('../data/*HW.R.P.contrast*')

# Arranging data in chronological order

KcontFiles = np.sort(KcontFiles)
RcontFiles = np.sort(RcontFiles)

# Capturing on limb intensities for each filter and observation time

files = [KcontFiles, RcontFiles]

int2 = []

for f in KcontFiles:
      
    data = fits.getdata(f)
   
    data1 = data[np.where(data >= -0.3)]
    
    int2.append(data1)
    
int3 = []


for f in RcontFiles:
        
    data2 = fits.getdata(f)
 
    data3 = data2[np.where(data2 >= -0.3)]
    
    int3.append(data3)
    
KcontInt = np.array(int2)
RcontInt = np.array(int3)

# Calculating first and second moments of on limb intensity distribution of both filters

mArray = np.zeros(6)
m1Array = np.zeros(6)

for i in range(len(KcontInt)):
        
    mean = np.sum(KcontInt[i]) / np.size(KcontInt[i])

    var = np.sqrt(np.sum((KcontInt[i] - mean)**2) / (np.size(KcontInt[i]) - 1))
   
    mArray[i] = mean
    m1Array[i] = var
   
mArray2 = np.zeros(6)
m1Array2 = np.zeros(6)

for i in range(len(RcontInt)):
    
    mean = np.sum(RcontInt[i]) / np.size(RcontInt[i])
   
    var = np.sqrt(np.sum((KcontInt[i] - mean)**2) / (np.size(KcontInt[i]) - 1))
   
    mArray2[i] = mean
    m1Array2[i] = var
    

print mArray
print mArray2
print m1Array
print m1Array2

save("../data/bruder_project3Cii.dat", ('meanArrayK', 'varArrayK', 'meanArrayR', 'varArrayR'), (mArray, m1Array, mArray2, m1Array2))
