from astropy.io import fits
import os
#import fimatch
from glob import glob
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm
from saverestore import *

#retrieving fits data

data1, hdr  = fits.getdata('../data/20050702.1702.HW.R.P.rdc.fits.gz', header=True)
data2, hdr  = fits.getdata('../data/20050702.1702.HW.R.P.contrast.fits.gz', header=True)


KrdcFiles = glob('../data/*HW.K.P.rdc*')
RrdcFiles = glob('../data/*HW.R.P.rdc*')
KcontFiles = glob('../data/*HW.K.P.contrast*')
RcontFiles = glob('../data/*HW.R.P.contrast*')

files = ['KrdcFiles', 'RrdcFiles', 'KcontFiles', 'RcontFiles']

juldateK = []
juldateR = []
avgWidthK = []
RMSAK = []
avgWidthR = []
RMSAR = []
    
for f in KrdcFiles:
    
    hdr = fits.getheader(f)

    juldateK.append(hdr['JULDATE'])

    avgWidthK.append(hdr['AVGWIDTH'])

    RMSAK.append(hdr['RMSA'])

    
for f in RrdcFiles:
    
    hdr = fits.getheader(f)

    juldateR.append(hdr['JULDATE'])

    avgWidthR.append(hdr['AVGWIDTH'])

    RMSAR.append(hdr['RMSA'])


juldateK = np.array(juldateK)
juldateR = np.array(juldateR)
avgWidthK = np.array(avgWidthK)
RMSAK = np.array(RMSAK)
avgWidthR = np.array(avgWidthR)
RMSAR = np.array(RMSAR)

orderK = juldateK.argsort()
orderR = juldateR.argsort()
juldate = juldateK[orderK]
avgWidthK = avgWidthK[orderK]
RMSAK = RMSAK[orderK]
avgWidthR = avgWidthR[orderR]
RMSAR = RMSAR[orderR]

print hdr

save("../data/bruder_project3Bi.dat", ('juldate', 'avgWidthK', 'RMSAK', 'avgWidthR', 'RMSAR'), (juldate, avgWidthK, RMSAK, avgWidthR, RMSAR))
