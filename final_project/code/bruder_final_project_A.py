from astropy.io import fits
import numpy as np
import matplotlib.pyplot as pl

pl.ion()
pl.close()

# Retrieving data

data = fits.getdata('../data/spec-0429-51820-0056.fits.gz')

data2 = np.genfromtxt('../data/linelist-0429-51820-0056.csv',dtype='float64', comments='#', delimiter=',', skip_header=1)

data2 = data2.T

flux = np.array([i[0] for i in data])

wavelength = np.array([10**i[1] for i in data])

refwavelength = data2[1]

# plots of data and ref wavelengths

for i in range(len(refwavelength)):
    pl.axvline(refwavelength[i], color = 'g')

pl.plot(wavelength, flux)
pl.xlim(3800, 9200)
pl.xlabel('wavelength (Angstroms)')
pl.ylabel('F/lambda (e-17 erg/s/cm^2/Ang)')
pl.title('Electromagnetic Spectrum')
pl.show()
pl.savefig('../images/bruder_final_projectAi.png')
pl.close()

for i in range(len(refwavelength)):
    pl.axvline(refwavelength[i], color = 'g')

pl.plot(wavelength, flux)
pl.xlim(6500, 7000)
pl.xlabel('wavelength (Angstroms)')
pl.ylabel('F/lambda (e-17 erg/s/cm^2/Ang)')
pl.title('Electromagnetic Spectrum')
pl.show()
pl.savefig('../images/bruder_final_projectAii.png')
pl.close()

for i in range(len(refwavelength)):
    pl.axvline(refwavelength[i] + 112, color = 'g')

pl.plot(wavelength, flux)
pl.xlim(6500, 7000)
pl.xlabel('wavelength (Angstroms)')
pl.ylabel('F/lambda (e-17 erg/s/cm^2/Ang)')
pl.title('Electromagnetic Spectrum')
pl.show()
pl.savefig('../images/bruder_final_projectAiii.png')
pl.close()

# analysizing the spacing between wavelength samples

stepsize = wavelength[1:] - wavelength[:-1]

max = np.max(stepsize)
max = np.max(stepsize)
min = np.min(stepsize)
avg = np.mean(stepsize)
sd = np.std(stepsize)

print max, min, avg, sd


