import numpy as np
import matplotlib.pyplot as plt
import pdb
from scipy.optimize import leastsq

def gaussian(x, params):
    """
    Function will produce a gaussian given a set of parameters

    Input
    -----
    x = independent x-values

    params = an array of list of the mean, standard deviation, amplitude and vertical offset.

    Example
    -------
    To produce a gaussian given a set of parameters:
    >>>  f = gaussian(x, params)

    Returns
    ------
    f = a set of corresponding dependent values of the gaussian function
    """

    f = (np.exp(-np.power(x-params[0], 2.) / (2. * np.power(params[1], 2.)))) / (params[1] * (np.sqrt(2. * np.pi))) * params[2] + params[3]

    return f

def min_gaussian(params, x, data):
    """
    Finds the absolute difference between the data and fitted gaussian

    Input
    -----
    params = an array or list of the mean, standard deviation, amplitude and vertical offset.

    x = independent x-values

    data = array of y-values of the function to be fitted

    Example
    -------
    To compute the difference between the data and fitted gaussian:
    >>>  diff = min_gaussian(params, x, data)

    Returns
    ------
    Array of the absolute difference between the dependent values and the gaussian
    function specified by params.
    """
    
    return data - gaussian(x, params)

def fit_gaussian(params, x, data):
    """
    Fits a set of y-values with a gaussian function

    Input
    -----
    params = an array or list of the mean, standard deviation, amplitude and vertical offset.

    x = independent x-values

    data = array of y-values of the function to be fitted

    Example
    -------
    To compute the best fit gaussian:
    >>>  fit = fit_gaussian(params, x, data)

    Returns
    -------
    array of parameters,  fit [0], (plus additional info that can be found in scipy.optimize leastsq) that best fit the function

    """

    paramsbest = leastsq(min_gaussian, params, args = (x, data), full_output=1)

    return paramsbest


