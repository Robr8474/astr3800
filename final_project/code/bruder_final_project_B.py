from astropy.io import fits
import numpy as np
import matplotlib.pyplot as pl
from functions import *
from gaussian_fit import *

pl.ion()
pl.close()

# Retrieving data

data = fits.getdata('../data/spec-0429-51820-0056.fits.gz')

data2 = np.genfromtxt('../data/linelist-0429-51820-0056.csv',dtype='float64', comments='#', delimiter=',', skip_header=1)

data2b = np.genfromtxt('../data/linelist-0429-51820-0056.csv',dtype='str', comments='#', delimiter=',', skip_header=1)

data2 = data2.T

elements = data2b.T[0]

flux = np.array([i[0] for i in data])

wavelength = np.array([10**i[1] for i in data])

refwavelength = data2[1]


def fit(restwave, shiftedwave, width, wavelength, flux):
    
    """
    Fits a section of the spectrum with a gaussian function and computes the redshift for that section 

    Input
    -----
    restwave = array of wavelengths of reference emission/absorption lines

    shiftedwave = array of reference wavelengths shifted by an estimated redshift

    width = approximate width of spectral line in spectra

    wavelength = array of wavelengths for spectral data

    flux = array of fluxes for spectral data

    Example
    -------
    To fit a section of the spectrum with a gaussian function and compute the redshift:
    >>>  p, x, f, z = fit(refwavelength, shiftedwave, 22., wavelength, flux)

    Returns
    -------
    p = array of best fit parameters
   
    x = x-values of fitted section

    f = y-values of fitted section

    z = redshift

    """
    params = np.array([shiftedwave, 3.5, 500., 300.])
    
    x = wavelength[np.where((wavelength >= shiftedwave - width) * (wavelength < shiftedwave + width))]

    data = flux[np.where((wavelength >= shiftedwave - width) * (wavelength < shiftedwave + width))]

    fit = fit_gaussian(params, x, data)

    f = gaussian(x, fit[0])

    z = (fit[0][0] - restwave) / restwave

    return fit, x, f, z

# shifted the reference wavelengths by the estimated value of z

shiftedwave = refwavelength * 0.017 + refwavelength

# computation of best fit gaussian for each reference wavelength and plots of results (first 2 reference wavelengths are out of the spectrum)

linewidth = []
redshift = []
params = []
fvalues = []
error = []

fig, axarr = pl.subplots(4, 3, figsize = (10, 10))
e = 0
for i in range(4):
    for j in range(3):
                
        p, x, f, z = fit(refwavelength[j+e+2], shiftedwave[j+e+2], 10., wavelength, flux)

        linewidth.append(p[0][1]) # standard deviation of the fit
        params.append(p[0]) # parameters of the fit
        redshift.append(z)
    
        axarr[i, j].plot(wavelength, flux)
        axarr[i, j].plot(x, f)
        axarr[i, j].set_ylabel('F/lambda')
        axarr[i, j].set_xlabel('Wavelength (Angstroms)')
        axarr[i, j].set_xlim(shiftedwave[j+e+2] - 30, shiftedwave[j+e+2] + 30)
        axarr[i, j].set_title(elements[j+e+2])

        # calculating the error in the mean of the fit from the covariance matrix
        
        try:
            error.append(np.sqrt(np.diag(p[1]))[0])
        except:
            error.append(1)

    e += 3
       
fig.tight_layout()
pl.savefig('../images/bruder_final_projectBi.png')
pl.show()
pl.close()

fig, axarr = pl.subplots(3, 3, figsize = (10, 10))

for i in range(3):
    for j in range(3):
                
        p, x, f, z = fit(refwavelength[j+e+2], shiftedwave[j+e+2], 10., wavelength, flux)

        linewidth.append(p[0][1])
        params.append(p[0])
        redshift.append(z)
    
        axarr[i, j].plot(wavelength, flux)
        axarr[i, j].plot(x, f)
        axarr[i, j].set_ylabel('F/lambda')
        axarr[i, j].set_xlabel('Wavelength (Angstroms)')
        axarr[i, j].set_xlim(shiftedwave[j+e+2] - 30, shiftedwave[j+e+2] + 30)
        axarr[i, j].set_title(elements[j+e+2])

        try:
            error.append(np.sqrt(np.diag(p[1]))[0])
        except:
            error.append(1)
            
    e += 3
       
fig.tight_layout()
pl.savefig('../images/bruder_final_projectBii.png')
pl.show()
pl.close()

# consolidating data

redshift = np.array(redshift)
params = np.array(params)
params = params.T

offsetwavelength = params[0]
linewidth = params[1]
amplitude = params[2]
vert = params[3]

error = np.array(error)

# selecting the reference wavelengths that correlate to an emission line in the spectrum

y = [7, 9, 12, 15, 16, 17, 18, 19]

redshift = redshift[y]

linewidth = linewidth[y]

# computation of uncertainty in mean of the fit

uncert_fit = np.sqrt(np.sum(((error[y] / offsetwavelength[y])**2))) / len(error)

# computation of uncertainty in z using the width of the fitted curve

uncert = []

uncert.append([((linewidth[i] / refwavelength[i])**2) for i in range(len(linewidth))])

sumof = np.sum(uncert)
sqrtof = np.sqrt(sumof)
uncert_width = sqrtof / len(linewidth)

print 'mean value of z =', np.mean(redshift)
print 'std deviation of z =', np.std(redshift) / (len(redshift) - 1)
print 'uncertainty in fit of z =', uncert_fit
print 'uncertainty using width of Gaussian fit =', uncert_width

# width of gaussian uncertainty variation with number of spectral lines used to compute z

uncertlist = []
sumof2 = 0.
for i in range(len(linewidth)):
    
    sumof2 += uncert[0][i]
    sqrtof2 = np.sqrt(sumof2)
    uncertainty2 = sqrtof2 / float(i + 1)
    uncertlist.append(uncertainty2)

xv = [1, 2, 3, 4, 5, 6, 7, 8]

pl.plot(xv, uncertlist)
pl.xlabel('Number of spectral lines')
pl.ylabel('Uncertainty in Redshift')
pl.title('Uncertainty Variation with Number of Spectral Lines Used')
pl.savefig('../images/bruder_final_projectBiii.png')
pl.show()
pl.close()

# uncertainty in mean of fit variation with number of spectral lines used

uncertlist_fit = []
err = []
e = 0
errory = error[y]

for i in range(len(linewidth)):

    err.append(e)
    
    unc = np.sqrt(np.sum(((errory[i] / offsetwavelength[i])**2))) / len(err)

    uncertlist_fit.append(unc)

    e += 1

xv = [1, 2, 3, 4, 5, 6, 7, 8]

pl.plot(xv, uncertlist_fit)
pl.xlabel('Number of spectral lines')
pl.ylabel('Uncertainty in Redshift')
pl.title('Uncertainty Variation with Number of Spectral Lines Used')
pl.savefig('../images/bruder_final_projectBiv.png')
pl.show()
pl.close()

# computation of uncertainty variation with fitted width of spectral line

width = []
uncert = []
lw = []
un = []
k = 5

y2 = [9, 11, 14, 17, 18, 19, 20, 21]
refwavelength2 = refwavelength[y2]
shiftedwave2 = shiftedwave[y2]

for i in range(30):
    for j in range(8):
                
        p, x, f, z = fit(refwavelength2[j], shiftedwave2[j], float(i+5), wavelength, flux)

        lw.append(p[0][1])
            
    uncert.append([((lw[i] / refwavelength2[i])**2) for i in range(len(lw))])
    un.append(np.sqrt(np.sum(uncert)) / len(lw))
    width.append(k)
    k += 1
    uncert = []
    lw = []
    
pl.plot(width, un)
pl.xlabel('Width of Spectral Lines')
pl.ylabel('Uncertainty in Redshift')
pl.title('Uncertainty Variation with Width of Spectrum Used')
pl.savefig('../images/bruder_final_projectBv.png')
pl.show()
pl.close()

# computation of sigma of z variation with number of spectral lines used

sig = []

sig.append([(np.std(redshift[0:i+2]) / (len(redshift[0:i+2]) - 1.)) for i in range(len(redshift))])

pl.plot(xv, sig[0])
pl.xlabel('Number of Lines Used')
pl.ylabel('Sigma in Redshift')
pl.title('Uncertainty Variation with Number of Spectral Lines Used')
pl.savefig('../images/bruder_final_projectBvi.png')
pl.show()
#pl.close()
